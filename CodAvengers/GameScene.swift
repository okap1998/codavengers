//
//  GameScene.swift
//  CodAvengers
//
//  Created by Pasquale Di Donna on 12/05/2020.
//  Copyright © 2020 Pasquale Di Donna. All rights reserved.
//

import UIKit
import SpriteKit

enum CollisionType: UInt32 {
    case player = 1
    case bonus = 2
    case malus = 4
    
}

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    var player: SKSpriteNode!
    var ButtonPause: SKSpriteNode!
    
    //   array con i nomi dei bonus e malus
    let bonus = ["MozziconeBonus", "LattinaBonus", "Vino"]
    let malus = ["PetrolioMalus"]

    var offset: CGFloat = 0
    var right: SKAction!
    var left: SKAction!
    var defaults = UserDefaults.standard
    var highscore:Int = 0
    var lastScore:Int = 0
    var BackGround = SKSpriteNode(imageNamed: "Menu")
    var bottoneQuit = SKSpriteNode(imageNamed: "BottoneQuit")
    var bottoneSetting = SKSpriteNode(imageNamed: "BottoneSettingPause" )
    var bottoneResume = SKSpriteNode(imageNamed: "BottoneResume")
    let generator = UINotificationFeedbackGenerator()
    
    var titlePauseLabel: SKLabelNode!
    var scoreLabel: SKLabelNode!
    var scoreLabelTitlePause: SKLabelNode!
    var ScoreLabelPause: SKLabelNode!
    var score = 0 {
        didSet {
            scoreLabel.text = "\(score)"
        }
    }
    
    override func didMove(to view: SKView) {
        physicsWorld.contactDelegate = self
  
        gameMovement()
        createScoreLabel()
        createGround()
        createPlayer()
        createTitlePause()
        
        createPause()
        createSettings()
//Controllo velocità
        infinity_spawn()
        
    }
    
    func gameMovement(){
        
        let offset = self.frame.size.width / 4;
        right = SKAction.moveBy(x: offset, y: 0, duration: 0.6)
        left = SKAction.moveBy(x: -offset, y: 0, duration: 0.6)
        let swipeRight:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipedRight))
        swipeRight.direction = .right
        view!.addGestureRecognizer(swipeRight)
        
        let swipeLeft:UISwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(swipedLeft))
        swipeLeft.direction = .left
        view!.addGestureRecognizer(swipeLeft)
    }
    
    override func update(_ currentTime: TimeInterval) {
        
     
    }
    func createTitlePause(){
        titlePauseLabel = SKLabelNode(fontNamed:"FredokaOne-Regular")
        titlePauseLabel.text = "PAUSE"
        titlePauseLabel.fontSize = 100
        titlePauseLabel.horizontalAlignmentMode = .center
        titlePauseLabel.fontColor = .white
        titlePauseLabel.position = CGPoint(x: frame.midX, y: self.frame.height - (150 + 60.5))
        titlePauseLabel.zPosition = 15
        addChild(titlePauseLabel)
        
        
        scoreLabelTitlePause = SKLabelNode(fontNamed:"FredokaOne-Regular")
        scoreLabelTitlePause.text = "SCORE"
        scoreLabelTitlePause.fontSize = 50
        scoreLabelTitlePause.horizontalAlignmentMode = .center
        scoreLabelTitlePause.fontColor = .white
        scoreLabelTitlePause.position = CGPoint(x: frame.midX, y: self.frame.height - (296 + 25))
        scoreLabelTitlePause.zPosition = 15
        addChild(scoreLabelTitlePause)
        
      
        
        titlePauseLabel.isHidden = true
        scoreLabelTitlePause.isHidden = true
   
        
        
        
    }
    func createScoreLabelPause(){
           
        ScoreLabelPause = SKLabelNode(fontNamed:"FredokaOne-Regular")
        ScoreLabelPause.text = "\(score)"
        ScoreLabelPause.fontSize = 75
        ScoreLabelPause.horizontalAlignmentMode = .center
        ScoreLabelPause.fontColor = .white
        ScoreLabelPause.position = CGPoint(x: frame.midX, y: self.frame.height - (350 + 37.5))
        ScoreLabelPause.zPosition = 11
        addChild(ScoreLabelPause)
        
        ScoreLabelPause.isHidden = true
           
           
       }
    
    func createScoreLabel(){
        
        scoreLabel = SKLabelNode(fontNamed:"FredokaOne-Regular")
        scoreLabel.text = "0"
        scoreLabel.horizontalAlignmentMode = .left
        scoreLabel.position = CGPoint(x: frame.size.width/2 - 160 , y: frame.size.height - 80)
        scoreLabel.zPosition = 11
        addChild(scoreLabel)
        
        
    }
    func createHighscore() {
        
        print("Score:\(score)")
        
        
        
        defaults.setValue(highscore, forKey: "highscore")
        print("Highscore:\(highscore)")
        
        if score > highscore {
            highscore = score
            defaults.setValue(highscore, forKey: "highscore")
            
        }
 
        
       }
    @objc func swipedRight(sender:UISwipeGestureRecognizer){
        if (player.position.x <= frame.size.width/2 + offset + 0.5){
            player.run(right)
        }
    }
    @objc func swipedLeft(sender:UISwipeGestureRecognizer){
        if (player.position.x >=  frame.size.width/2 - offset - 0.5){
            player.run(left)
        }
    }
    
    func createPlayer() {
        player = SKSpriteNode(imageNamed: "greta")
        player.position = CGPoint(x: self.frame.width/2, y: self.frame.height / 2  - player.size.height - 120)
        player.zPosition = 8
        player.size = CGSize(width: player.size.width/1.5 , height: player.size.height/1.5)
        
        player.physicsBody = SKPhysicsBody(texture: player.texture!, size: player.texture!.size())
        player.physicsBody?.categoryBitMask = CollisionType.player.rawValue
        player.physicsBody?.collisionBitMask = 0
        player.physicsBody?.contactTestBitMask = CollisionType.bonus.rawValue | CollisionType.malus.rawValue
        player.physicsBody?.isDynamic = false

        addChild(player)
    }
    
    func infinity_spawn(){
        
        print("Velocita MINIMA")
       
        run(SKAction.repeatForever(
            SKAction.sequence([
                
                SKAction.run(spawnObject),
                SKAction.wait(forDuration: 1.5)
                
            ])
            ),
            withKey: "spawnEnemy")
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)  {
        let touch = touches.first
        let positionInScene = touch!.location(in: self)
        let touchedNode = self.atPoint(positionInScene)
        
        if let name = touchedNode.name {
            if name == "BottonePausa" {
                self.scene?.isPaused = true
                
                ButtonPause.isHidden = true
                scoreLabel.isHidden = true
                player.isHidden = true
                bottoneQuit.isHidden = false
                bottoneResume.isHidden = false
                bottoneSetting.isHidden = false
                BackGround.isHidden = false
                titlePauseLabel.isHidden = false
                scoreLabelTitlePause.isHidden = false

                
                createScoreLabelPause()
                ScoreLabelPause.isHidden = false
                
       
               }
            
            if name == "BottoneResume"{
                
                
                BackGround.isHidden = true
                bottoneSetting.isHidden = true
                bottoneQuit.isHidden = true
                bottoneResume.isHidden = true
                player.isHidden = false
                ButtonPause.isHidden = false
                scoreLabel.isHidden = false
                titlePauseLabel.isHidden = true
                scoreLabelTitlePause.isHidden = true
                ScoreLabelPause.isHidden = true
                scene?.isPaused = false
            
            
            
            
           }
        }
    }
    func createSettings(){
        BackGround.size = CGSize(width: frame.size.width, height: frame.size.height)
        BackGround.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
     
        
        bottoneResume.name = "BottoneResume"
        bottoneResume.position = CGPoint(x: self.frame.midX, y: frame.size.height - (480 + 37.5 ))
        bottoneResume.size = CGSize(width: 325, height: 76)
        
        bottoneQuit.name = "BottoneQuit"
        bottoneQuit.position = CGPoint(x: self.frame.midX, y: frame.size.height - (680 + 37.5 ))
        bottoneQuit.size = CGSize(width: 325, height: 76)
        
        bottoneSetting.name = "BottoneSetting"
        bottoneSetting.position = CGPoint(x: self.frame.midX, y: frame.size.height - (580 + 37.5 ))
        bottoneSetting.size = CGSize(width: 325, height: 76)
        
        bottoneSetting.zPosition = 11
        bottoneQuit.zPosition = 11
        bottoneResume.zPosition = 11
        BackGround.zPosition = 11
        
        addChild(BackGround)
        addChild(bottoneSetting)
        addChild(bottoneQuit)
        addChild(bottoneResume)
        
        
        bottoneQuit.isHidden = true
        bottoneResume.isHidden = true
        bottoneSetting.isHidden = true
        BackGround.isHidden = true
    }
    
    
    func spawnObject() {
        
        let boolean = Bool.random()
        if boolean {
            let b = bonus.randomElement()
            let node = SKSpriteNode(imageNamed: b!)
            node.name = b
            node.zPosition = 5
            node.physicsBody = SKPhysicsBody(circleOfRadius: node.size.width / 4)
            node.physicsBody?.categoryBitMask = CollisionType.bonus.rawValue
            node.physicsBody?.contactTestBitMask = CollisionType.player.rawValue
            node.physicsBody?.collisionBitMask = 0
            node.physicsBody?.affectedByGravity = false
            
            let val = Int.random(in: 0..<3)
            node.position = CGPoint(x: frame.size.width/2 + frame.size.width/4 * (-1 + CGFloat(val)), y: frame.size.height + node.size.height/2)
            addChild(node)
            
            node.run(SKAction.moveBy(x: 0, y: (-size.height - node.size.height) * 2, duration: 8))
            
            
        }
        else {
            let m = malus.randomElement()
            let node = SKSpriteNode(imageNamed: m!)
            node.name = m
            node.zPosition = 5
            node.physicsBody = SKPhysicsBody(circleOfRadius: node.size.width / 4)
            node.physicsBody?.categoryBitMask = CollisionType.malus.rawValue
            node.physicsBody?.contactTestBitMask = CollisionType.player.rawValue
            node.physicsBody?.collisionBitMask = 0
            node.physicsBody?.affectedByGravity = false
            let val = Int.random(in: 0..<3)
            node.position = CGPoint(x: frame.size.width/2 + frame.size.width/4 * (-1 + CGFloat(val)), y: frame.size.height + node.size.height/2)
            addChild(node)
            
            node.run(SKAction.moveBy(x: 0, y: (-size.height - node.size.height) * 2, duration: 8))
            
//            print(val)
        }
        
    }
    
    func createPause(){
        
        ButtonPause = SKSpriteNode(imageNamed: "BottonePause")
        ButtonPause.position = CGPoint(x: frame.size.width/2 - 100 , y: frame.size.height - 69)
        ButtonPause.zPosition = 10
        ButtonPause.name = "BottonePausa"
        addChild(ButtonPause)
       }
    
    func didBegin(_ contact: SKPhysicsContact) {
       
        if contact.bodyA.node == player {
            playerCollided(with: contact.bodyB.node!)
            
        } else if contact.bodyB.node == player {
            playerCollided(with: contact.bodyA.node!)
            
        }
    }
    
       func playerCollided(with node: SKNode) {
            print("playerCollided")
    //        cambiati gli if
            if malus.firstIndex(of: node.name!) != nil {
                lastScore = score
                defaults.setValue(lastScore, forKey: "lastScore")
                
                node.removeFromParent()
                generator.notificationOccurred(.warning)
                let nextScene = GameOverScene(size: self.size)
                self.view?.presentScene(nextScene, transition: .crossFade(withDuration: 1))
                
            } else if bonus.firstIndex(of: node.name!) != nil {
                node.removeFromParent()
                score += 10
                generator.notificationOccurred(.success)
                
                createHighscore()
            }
        }
    
    
    func createGround() {
        let background = SKSpriteNode(imageNamed: "Sfondo")
        background.size = CGSize(width: (self.scene?.size.width)!, height:(self.scene?.size.height)!)
        background.size = CGSize(width: frame.size.width, height: frame.size.height)
        background.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        background.zPosition = -1
        addChild(background)
        
        let groundTexture = SKTexture(imageNamed: "Foresta")
        
        for i in 0 ... 2 {
            
            let ground = SKSpriteNode(texture: groundTexture)
            
            ground.size = CGSize(width: (self.scene?.size.width)!, height:ground.size.height)
            
            ground.zPosition = 9
            
            ground.position = CGPoint(x: frame.size.width/2 , y:  ((groundTexture.size().height * CGFloat(i))))
            addChild(ground)
            
            let moveDown = SKAction.moveBy(x: 0 , y: -groundTexture.size().height, duration: 8)
            let moveReset = SKAction.moveBy(x: 0 , y: groundTexture.size().height, duration: 0)
            let moveLoop = SKAction.sequence([moveDown, moveReset])
            let moveForever = SKAction.repeatForever(moveLoop)
            
            ground.run(moveForever)
        }
    }
    
    
}
