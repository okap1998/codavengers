//
//  GameViewController.swift
//  CodAvengers
//
//  Created by Pasquale Di Donna on 12/05/2020.
//  Copyright © 2020 Pasquale Di Donna. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit

class GameViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    
  
    let scene = SceneMenu(size: view.bounds.size)
    let skView = view as! SKView
    skView.showsFPS = false
    skView.showsNodeCount = false
    skView.ignoresSiblingOrder = false
    skView.presentScene(scene)
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
}
