//
//  SceneMenu.swift
//  Prova3
//
//  Created by Alessio Vetrano on 18/05/2020.
//  Copyright © 2020 Ada. All rights reserved.
//

import UIKit
import SpriteKit

class SceneMenu: SKScene {
    
    let buttonPlay = SKSpriteNode(imageNamed: "BottonePlay")
    let buttonSettings = SKSpriteNode(imageNamed: "BottoneSetting")
    let buttonDonate = SKSpriteNode(imageNamed: "BottoneDonate")
    let backGroundHome = SKSpriteNode(imageNamed: "backGroundHome")
    var GretaTitle: SKLabelNode!
    var BestScoreLabel: SKLabelNode!
    var HighscoreLabel: SKLabelNode!
    var HighScoreUserDefault = UserDefaults.standard.value(forKey: "highscore")as? Int
    
    
    override func didMove(to view: SKView) {
        GretaTitle = SKLabelNode(fontNamed:"FredokaOne-Regular")
        GretaTitle.text = "GRETA"
        GretaTitle.fontSize = 100
        
        GretaTitle.horizontalAlignmentMode = .center
        GretaTitle.position = CGPoint(x: frame.midX, y: self.frame.height - (150 + 60.5))
        GretaTitle.zPosition = 6
        addChild(GretaTitle)
        
        BestScoreLabel = SKLabelNode(fontNamed:"FredokaOne-Regular")
        BestScoreLabel.text = "BEST SCORE"
        BestScoreLabel.fontSize = 50
        BestScoreLabel.horizontalAlignmentMode = .center
        BestScoreLabel.position = CGPoint(x: frame.midX, y: self.frame.height - (296 + 30.5))
        BestScoreLabel.zPosition = 6
        addChild(BestScoreLabel)
        
        HighscoreLabel = SKLabelNode(fontNamed:"FredokaOne-Regular")
        
        if HighScoreUserDefault == nil {
            HighscoreLabel.text = "0"
        } else {
        HighscoreLabel.text = "\(HighScoreUserDefault!)"
        }
        HighscoreLabel.fontSize = 75
        HighscoreLabel.horizontalAlignmentMode = .center
        HighscoreLabel.position = CGPoint(x: frame.midX, y: self.frame.height - (357 + 30.5))
        HighscoreLabel.zPosition = 6
        addChild(HighscoreLabel)
               
        
        backGroundHome.size = CGSize(width: view.bounds.width, height: view.bounds.height)
        buttonPlay.name = "BottonePlay"
        buttonSettings.name = "BottoneSetting"
        buttonDonate.name = "BottoneDonate"
        backGroundHome.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
    
        buttonSettings.position = CGPoint(x: self.frame.midX, y: frame.size.height - (580 + 37.5 ))
        buttonPlay.position = CGPoint(x: self.frame.midX,y: frame.size.height - (480 + 37.5))
        buttonDonate.position = CGPoint(x: self.frame.midX, y: frame.size.height - (680 + 37.5))
        
        
       
        addChild(backGroundHome)
        addChild(buttonDonate)
        addChild(buttonSettings)
        self.addChild(buttonPlay)
        
       
      }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)  {
        let touch = touches.first
        let positionInScene = touch!.location(in: self)
        let touchedNode = self.atPoint(positionInScene)
        
      

    if let name = touchedNode.name {
        if name == "BottonePlay" {
            
            let nextScene = GameScene(size: self.size)
            self.view?.presentScene(nextScene, transition: .crossFade(withDuration: 1))

          }
        
        if name == "BottoneSetting" {
            print("Setting")
        }
        if name == "BottoneDonate" {
            print("Donate")
        }
        
        
        
      }
   }
}
