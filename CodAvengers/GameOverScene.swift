//
//  GameOverScene.swift
//  Prova3
//
//  Created by Alessio Vetrano on 18/05/2020.
//  Copyright © 2020 Ada. All rights reserved.
//

import UIKit
import SpriteKit

class GameOverScene: SKScene {
 
    
    var lastScoreUserDefault = UserDefaults.standard.value(forKey: "lastScore") as? Int
    var GameOverBackGround: SKSpriteNode!
    var GameOverFinestra: SKSpriteNode!
    let buttonQuit = SKSpriteNode(imageNamed: "Quit2")
    let buttonResume = SKSpriteNode(imageNamed: "BottoneRestart")
    var lastScoreLabel: SKLabelNode!
    var GameOverTitle: SKLabelNode!
    
    override func didMove(to view: SKView) {
        
        createButtons()
        createLastScore()
        createBackGround()

    }
   
    func createBackGround(){
        
        GameOverBackGround = SKSpriteNode(imageNamed: "SfondoGameOver")
        GameOverBackGround.size = CGSize(width: frame.size.width, height: frame.size.height)
        GameOverBackGround.position = CGPoint(x: frame.size.width / 2, y: frame.size.height / 2)
        GameOverFinestra = SKSpriteNode(imageNamed: "FinestraGameOver")

        GameOverFinestra.position = CGPoint(x: frame.size.width / 2, y: frame.size.height - (376 + 260))
        
        addChild(GameOverBackGround)
        addChild(GameOverFinestra)
        
        
    }
    
    func createLastScore(){
        lastScoreLabel = SKLabelNode(fontNamed:"FredokaOne-Regular")
        lastScoreLabel.fontSize = 50
        lastScoreLabel.horizontalAlignmentMode = .center
        lastScoreLabel.position = CGPoint(x: frame.midX, y: self.frame.height - (620 + 30.5))
        lastScoreLabel.fontColor = UIColor(red: 127/255  , green: 182/255 , blue: 132/255 , alpha: 1)
        lastScoreLabel.zPosition = 6
        addChild(lastScoreLabel)
        if lastScoreUserDefault == nil {
            lastScoreLabel.text = "0"
        } else {
            lastScoreLabel.text = "SCORE: \(lastScoreUserDefault!)"
        }
        
        
    }
    
    
    func createButtons(){
        
        buttonResume.name = "BottoneRestart"
        buttonResume.position = CGPoint(x: self.frame.midX, y: frame.size.height - (670 + 37.5 ))
        buttonQuit.name = "BottoneQuit"
        buttonQuit.position = CGPoint(x: self.frame.midX, y: frame.size.height - (771 + 37.5 ))
        buttonResume.zPosition = 5
        buttonQuit.zPosition = 5
        addChild(buttonQuit)
        addChild(buttonResume)
        
        
    }
    
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)  {
        let touch = touches.first
        let positionInScene = touch!.location(in: self)
        let touchedNode = self.atPoint(positionInScene)
        
        if let name = touchedNode.name {
            if name == "BottoneRestart" {
                
                
                let nextScene = GameScene(size: self.size)
                self.view?.presentScene(nextScene, transition: .doorsOpenHorizontal(withDuration: 1))
                
                
            }
            
            if name == "BottoneRevive" {
                print("Revive")
            }
 
        }
        
    }
    
    
    
}
